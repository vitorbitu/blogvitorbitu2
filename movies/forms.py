from django.forms import ModelForm
from .models import Movie, Review


class MovieForm(ModelForm):
    class Meta:
        model = Movie
        fields = [
            'name',
            'text',
            'poster_url',
        ]
        labels = {
            'name': 'Título',
            'text': 'Texto do post',
            'poster_url': 'URL do Poster',
        }


class ReviewForm(ModelForm):
    class Meta:
        model = Review
        fields = [
            'text',
        ]
        labels = {
            'text': 'Comentário',
        }

