from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.forms import UserCreationForm


def login(request):
    if request.method == 'POST':
        m = User.objects.get(username=request.POST['username'])
        if m.password == request.POST['password']:
            request.session['user_id'] = m.id
            return HttpResponseRedirect(reverse('users:logged'))
        else:
            return HttpResponse("O nome e senha não correspondem.")

    form = UserForm()
    context = {'form': form}
    return render(request, 'users/login.html', context)


def logged(request):
  if request.session.get('user_id', False):
    return HttpResponse(f'Você está logado como usuário com id {user_id}.')
  return HttpResponse('Faça o login para ver esta página.')

def logout(request):
    try:
        del request.session['member_id']
    except KeyError:
        pass
    return HttpResponseRedirect(reverse('users:logged'))


def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('index'))
    else:
        form = UserCreationForm()

    context = {'form': form}
    return render(request, 'accounts/signup.html', context)